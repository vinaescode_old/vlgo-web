<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <style>
        *
        {
            margin: 0 auto;
            padding: 0 0 0 0;
        }

        .sidebar
        {
            position: fixed;
            background-color: #000;
            height: 100%;
            width: 200px;
            z-index: 99;
        }

        .event-wrapper
        {
            background-color: #ff0200;
            margin-left: 200px;
            max-width: 90%;
        }

        .event-block
        {
            float: left;
            vertical-align: bottom;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            max-height: 135px;
            max-width: 230px;
        }

        .event-img
        {
            max-height: 300px;
            max-width: 500px;
        }

        .event-title
        {
            color: #fff;
            max-width: 250px;
            max-height: 19px;
            font-size: 12px;
        }

        .title-block
        {
            vertical-align: bottom;
            background-color: rgba(0, 0, 0, 0.6);
            min-width: 230px;
            text-align: center;
            margin-top: 115px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="sidebar"></div>
    <div class="event-wrapper">
<?php

ini_set('max_execution_time', 0);
header('Content-Type:text/html; charset=utf-8');

require 'Inner/Event.php';
require 'Inner/EventParser.php';
require 'Inner/Vk.php';

$vk = new Vk();
$vk->setToken('d0c6149c7b6aeda798323ab36c010efc97a714219c44746a313beb49b75e4718176124dfd7f355ebb1253');

$ep = new EventParser($vk);
$c=$ep->getAll();

$eventblock = file_get_contents('test.html');
$all = file_get_contents('SUPER.html');

$buffer = '';

ob_start();
foreach($c as $l)
{
    foreach($l as $d)
    {
        $img = $d->photo;
        $title = $d->titlePrediction;
        $buffer .= require('test.html');
    }
}
ob_flush();
ob_clean();
?>
    </div>
</div>

</body>
</html>