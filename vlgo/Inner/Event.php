<?php

class Event
{

    public $title;
    public $description;
    public $activity;
    public $startdate;
    public $enddate;
    public $photo;
    public $mapbox = false;
    public $address;
    public $titlePrediction;
    public $membersCount;
    public $tags;

    public function create($data) {
        if($data instanceof stdClass) {
            $this->title = !isset($data->name)?:$data->name;
            $this->description = !isset($data->description)?:$data->description;
            $this->titlePrediction = $this->getPrediction($this->title);
            $this->activity = !isset($data->activity)?:$data->activity;
            $this->startdate = !isset($data->start_date)?:$data->start_date;
            $this->enddate = !isset($data->finish_date)?:$data->finish_date;
            $this->photo = !isset($data->photo_max_orig)?:$data->photo_max_orig;
            $this->mapbox = !isset($data->place)?:$this->buildMapbox($data->place);
            $this->address = !isset($data->place->address)?:$data->place->address;
            $this->membersCount = !isset($data->members_count)?:$data->members_count;
            $this->last = (time() < $this->startdate)?false:true;
        }

        return $this;
    }

    public static function instance() {
        return new self;
    }

    public function getPrediction($data)
    {
        $tags = $this->getTags($data);
        $this->tags[] = '*';

        $t = '';

        foreach($tags as $c=>$v) {
            $t .= $c.',';
            $this->tags[] = $c;
        }

        return trim($t, ',');
    }

    public function buildMapbox($place = false) {
        if(!$place || $place === null) return false;

        //http://api.tiles.mapbox.com/v3/getjump.hl6gpjll/pin-s-triangle+f44(65.34,55.44,13)/65.34,55.44,13/500x500.png
        return sprintf('http://api.tiles.mapbox.com/v3/vkmaps.map-an1xcr4f/pin-s-triangle+f44(%1$s,%2$s,16)/%1$s,%2$s,16/500x500.png',
            $place->longitude,
            $place->latitude
        );
    }

    function getTags($data) {

        $test=$this->getKeywords($data);
        asort($test);

        return $test;
    }

    function getKeywords($contents,$symbol=5,$words=35){
        $contents = @preg_replace(array("'<[\/\!]*?[^<>]*?>'si","'([\r\n])[\s]+'si","'&[a-z0-9]{1,6};'si","'( +)'si"),
            array("","\\1 "," "," "),strip_tags($contents));
        $rearray = array("~","!","@","#","$","%","^","&","*","(",")","_","+",
            "`",'"',"№",";",":","?","-","=","|","\"","\\","/",
            "[","]","{","}","'",",",".","<",">","\r\n","\n","\t","«","»");

        $adjectivearray = array("ые","ое","ие","ий","ая","ый","ой","ми","ых","ее","ую","их","ым",
            "как","для","что","или","это","этих",
            "всех","вас","они","оно","еще","когда",
            "где","эта","лишь","уже","вам","нет",
            "если","надо","все","так","его","чем",
            "при","даже","мне","есть","только","очень",
            "сейчас","точно","обычно"
        );


        $contents = @str_replace($rearray," ",$contents);
        $keywordcache = @explode(" ",$contents);
        $rearray = array();

        foreach($keywordcache as $word){
            if(strlen($word)>=$symbol && !is_numeric($word)){
                $adjective = substr($word,-2);
                if(!in_array(mb_strtolower($adjective, "UTF-8"),$adjectivearray) && !in_array(mb_strtolower($word, "UTF-8"),$adjectivearray)){
                    $rearray[mb_strtolower($word, "UTF-8")] = (array_key_exists(mb_strtolower($word, "UTF-8"),$rearray)) ? ($rearray[mb_strtolower($word, "UTF-8")] + 1) : 1;
                }
            }
        }

        @arsort($rearray);
        $keywordcache = @array_slice($rearray,0,$words);
        $keywords = "";

        return $keywordcache;
    }
}