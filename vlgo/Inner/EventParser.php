<?php

function implodeCount($array, $glue, $from, $to)
{
    $buffer = '';
    for($i = $from; $i < $to; $i++)
    {
        if(!isset($array[$i])) continue;
        if($i==0)
        {
            $buffer.=$array[$i];
        }

        $buffer.=$glue.$array[$i];
    }

    return $buffer;
}

class EventParser
{
    /**
     * @var Vk
     */
    public $vk;

    public function __construct(Vk $vk) {
        $this->vk = $vk;
    }

    public function getByKeyword($keyword) {
        $vk = &$this->vk;
        $data=[];
        $events=$vk->request('groups.search', ['type' => 'event', 'q' => $keyword, 'v' => 5.5, 'count' => 1000]);

        $groups = [];

        foreach($events->response->items as $event)
        {
            $groups[] = $event->id;
        }

        $count = 0;

        for($i = 0; $i < sizeof($events->response->items); $i = $i + 500)
        {
            //mad vk coders, should add to docs that restriction .i.
            $eventsData=$vk->request('groups.getById', ['group_ids' => implodeCount($groups, ',', $i, 500+$i), 'fields' => 'description,photo_max_orig,activity,status,start_date,finish_date,city,country,place,members_count', 'v' => 5.5, 'offset' => $i]);
            foreach($eventsData->response as $ed)
            {
                $data[] = Event::instance()->create($ed);
            }
        }

        return $data;
    }

    public function getAll()  {
        $d=[]; foreach(['Владивосток', 'Vladivostok', 'vl', 'vdk', 'двфу', 'dvfu', 'fefu', 'вгуэс'] as $e) $d[]=$this->getByKeyword($e); return $d;
    }
}