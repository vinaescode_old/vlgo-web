<?php

class Vk {

    private $accessToken;

    public function __construct($token = false) {
        if($token) $this->setToken($token);
    }

    public function setToken($token) {
        $this->accessToken = $token;
    }

    public function request($method, $args = []) {
        $args['access_token'] = $this->accessToken;
        $args['lang'] = 'en';
        $args['v'] = '5.7';
        return json_decode(Http::post("https://api.vk.com/method/$method", $args));
    }
}

class Http {
    public static function get($url, $cookies = null) {
        return file_get_contents($url);
    }

    public static function post($url, $args = [], $headers = "Content-Type:application/x-www-form-urlencoded\r\n") {
        $context = stream_context_create([
            'http' => [
                'method' => 'POST',
                'header'  => $headers,
                'content' => http_build_query($args),
            ]
        ]);

        return file_get_contents($url, false, $context);
    }
}