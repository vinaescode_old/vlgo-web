<?php

ini_set('max_execution_time', 0);
header('Content-Type:text/html; charset=utf-8');

require '../Inner/Event.php';
require '../Inner/EventParser.php';
require '../Inner/Vk.php';

$vk = new Vk();
$vk->setToken('d0c6149c7b6aeda798323ab36c010efc97a714219c44746a313beb49b75e4718176124dfd7f355ebb1253');

$ep = new EventParser($vk);
$c=$ep->getAll();

$data=array();

foreach($c as $l)
{
    foreach($l as $d)
    {
        $data[] = $d;
    }
}

usort($data, function($d1, $d2) {
    if ($d1->membersCount == $d2->membersCount) {
        return 0;
    }

    return ($d1->membersCount < $d2->membersCount) ? 1 : -1;
});

file_put_contents('data.json', json_encode($data, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE));
?>