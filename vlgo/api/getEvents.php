<?php

$offset = !isset($_GET['offset'])?0:$_GET['offset'];
$count = !isset($_GET['count'])?40:$_GET['count'];
$filter =  !isset($_GET['filter'])?'*':$_GET['filter'];

$data=json_decode(file_get_contents('data.json'));

$pseudo = [];

$it = -1;

foreach($data as $k => $d)
{
    if($it >= $count+$offset)
    {
        break;
    }

    if($it < $offset)
    {
        $it++;
        continue;
    }

    if(in_array($filter, $d->tags))
    {
        $pseudo['items'][] = $d;
        $it++;
    }
}

$pseudo['count'] = sizeof($data);

if(isset($filter))
{
    $pseudo['filter'] = $filter;
}

print json_encode($pseudo);
