<?php

header('Content-Type: text/html; charset=utf-8');

$data=json_decode(file_get_contents('data.json'));

$tags = [];
$tagsFixed = [];

$SHIETS = ['владивосток', 'владивостоке', 'vladivostok', '*', 'владивостока'];

foreach($data as $d)
{
    foreach($d->tags as $t)
    {
        isset($tags[$t]) ? $tags[$t]++ : $tags[$t] = 1;
    }
}

$sum = 0;
$count = sizeof($tags);

foreach($tags as $t)
{
    $sum += $t;
}

$avg = $sum/$count;

foreach($tags as $c => $t)
{
    if($t > $avg && !in_array($c, $SHIETS))
    {
        $tagsFixed[$c] = $t;
    }
}

print json_encode($tagsFixed, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);