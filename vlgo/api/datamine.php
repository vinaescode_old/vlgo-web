<?php

header('Content-Type: text/html; charset=utf-8');

$data=json_decode(file_get_contents('data.json'));

$test = [];

var_dump(seokeywords($data[0]->title));

asort($test);

print_r($test);

function seokeywords($contents,$symbol=5,$words=35){
$contents = @preg_replace(array("'<[\/\!]*?[^<>]*?>'si","'([\r\n])[\s]+'si","'&[a-z0-9]{1,6};'si","'( +)'si"),
    array("","\\1 "," "," "),strip_tags($contents));
	$rearray = array("~","!","@","#","$","%","^","&","*","(",")","_","+",
    "`",'"',"№",";",":","?","-","=","|","\"","\\","/",
    "[","]","{","}","'",",",".","<",">","\r\n","\n","\t","«","»");

	$adjectivearray = array("ые","ое","ие","ий","ая","ый","ой","ми","ых","ее","ую","их","ым",
        "как","для","что","или","это","этих",
        "всех","вас","они","оно","еще","когда",
        "где","эта","лишь","уже","вам","нет",
        "если","надо","все","так","его","чем",
        "при","даже","мне","есть","только","очень",
        "сейчас","точно","обычно"
    );


	$contents = @str_replace($rearray," ",$contents);
	$keywordcache = @explode(" ",$contents);
	$rearray = array();

	foreach($keywordcache as $word){
        if(strlen($word)>=$symbol && !is_numeric($word)){
            $adjective = substr($word,-2);
            if(!in_array(mb_strtolower($adjective, "UTF-8"),$adjectivearray) && !in_array(mb_strtolower($word, "UTF-8"),$adjectivearray)){
                $rearray[mb_strtolower($word, "UTF-8")] = (array_key_exists(mb_strtolower($word, "UTF-8"),$rearray)) ? ($rearray[mb_strtolower($word, "UTF-8")] + 1) : 1;
            }
        }
    }

	@arsort($rearray);
	$keywordcache = @array_slice($rearray,0,$words);
	$keywords = "";

    return $keywordcache;
}