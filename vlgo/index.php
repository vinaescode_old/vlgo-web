<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script type="text/javascript" src="http://code.jquery.com/jquery-2.1.0.min.js"></script>
    <link rel="stylesheet" href="styles.css"/>
    <script>

        var filter = '*';
        var currentOffset = 0;
        var size = 0;

        $(document).ready(function() {
            $.get('api/getTags.php', function(data) {
                $.each(data, function(i, v, k) {
                    $('#addhere').append('<li><a class="filterable" id="'+ i +'" href="#"><p>' + i + '</p></a></li>');
                });

                $("a.filterable").click(function() {
                    console.log(this);
                    filter = this.id;
                    $(".event-wrapper").html('');
                    getFilteredData();
                });
            }, 'json');
            $.get('api/getEvents.php', {filter: filter}, function(data) {
                size = data.count;
                $.each(data.items, function(i, v) {
                    buildBlock(v);
                });
                currentOffset += 40;
            }, 'json');

            $("#last").change(function() {
                
            });
        });

        function getFilteredData() {
            $.get('api/getEvents.php', {filter: filter}, function(data) {
                size = data.count;
                $.each(data.items, function(i, v) {
                    buildBlock(v);
                });
                currentOffset = 0;
            }, 'json');
        }

        $(window).scroll(function() {
            if($(window).scrollTop() == $(document).height() - $(window).height()) {
                if(currentOffset > size) return;
                $.get('api/getEvents.php', {offset : currentOffset, filter: filter}, function(data) {
                    $.each(data.items, function(i, v) {
                        buildBlock(v);
                    });
                    currentOffset += 40;
                }, 'json');
            }
        });

        function buildBlock(data)
        {
            var style = "event-block";
            if(data.last)
            {
                style = style + " event-last";
            }
            $('.event-wrapper').append('<div class="'+ style +'" style="background-image: url('+ data.photo +')"><div class="title-block"><span class="event-title">'+ data.title +'</span></div></div>')
        }


    </script>
</head>
<body>
<div class="container">
    <div class="crutch">
        <div class="sidebar">
            <div class="category">
                <h4>Категории</h4>
                <ul id="addhere">
                    <li><a class="filterable" href="#" id="*"><p>Всё</p></a></li>
                </ul>
            </div>
            <div class="filters">
                <form method="GET">
                    <h4>Фильтры</h4>
                    <table>
                        <tbody><tr>
                            <td><input type="checkbox" id="last"></td><td><p>Прошедшие</p></td>
                        </tr>
                        </tbody></table>
                </form>
            </div>
        </div>
    </div>
    <div class="event-wrapper"></div>
</body>
</html>